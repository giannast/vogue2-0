//
//  IssueCollectionViewCell.swift
//  vogue2.0
//
//  Created by Gianna Stylianou on 12/12/19.
//  Copyright © 2019 Gianna Stylianou. All rights reserved.
//

import UIKit

class IssuesCollectionViewCell: UICollectionViewCell {

        @IBOutlet weak var imageCover: UIImageView!
        @IBOutlet weak var buyButton: UIButton!
        @IBOutlet weak var downloadButton: UIButton!
        @IBOutlet weak var sampleButton: UIButton!
        @IBOutlet weak var issueTitle: UILabel!
    
 var issue: Issue? {
        didSet {
            self.updateUI()
        }
    }
    
    private func updateUI()
    {
        if let issueObject = issue {
            imageCover.image = issueObject.issueCover
            issueTitle.text = issueObject.issueTitle
        } else {
            imageCover.image = nil
            issueTitle.text = nil
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        self.layer.cornerRadius = 3.0
        imageCover.layer.shadowRadius = 10
        imageCover.layer.shadowOpacity = 0.4
        imageCover.layer.shadowOffset = CGSize(width: 5, height: 10)
        buyButton.layer.cornerRadius = 17
        buyButton.backgroundColor = UIColor.black
//        buyButton.layer.borderWidth = 1
//        buyButton.layer.borderColor = UIColor.black.cgColor
        downloadButton.layer.cornerRadius = 17
        downloadButton.backgroundColor = UIColor.white
        downloadButton.layer.borderWidth = 1
        downloadButton.layer.borderColor = UIColor.black.cgColor
        sampleButton.layer.cornerRadius = 17
        sampleButton.backgroundColor = UIColor.white
        sampleButton.layer.borderWidth = 1
        sampleButton.layer.borderColor = UIColor.black.cgColor
        
        self.clipsToBounds = false
    }
}
