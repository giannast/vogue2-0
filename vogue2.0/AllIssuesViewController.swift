//
//  AllIssues.swift
//  vogue2.0
//
//  Created by Gianna Stylianou on 11/12/19.
//  Copyright © 2019 Gianna Stylianou. All rights reserved.
//
import UIKit

class AllIssuesViewController: UIViewController {
        
  @IBOutlet weak var collectionView: UICollectionView!
            
    var issues = Issue.fetchIssues()
    let cellScaling: CGFloat = 1
            
            override func viewDidLoad() {
                super.viewDidLoad()

                let screenSize = UIScreen.main.bounds.size
                let cellWidth = floor(screenSize.width * cellScaling)
                let cellHeight = floor(screenSize.height * cellScaling)

                let insetX = (view.bounds.width - cellWidth) / 2.0
                let insetY = (view.bounds.height - cellHeight) / 2.0

                let layout = collectionView!.collectionViewLayout as! UICollectionViewFlowLayout
                layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
                collectionView?.contentInset = UIEdgeInsets(top: insetY, left: insetX, bottom: insetY, right: insetX)
                
                collectionView?.dataSource = self
                collectionView?.delegate = self
            }
        }

        extension AllIssuesViewController : UICollectionViewDataSource
        {
            func numberOfSections(in collectionView: UICollectionView) -> Int {
                return 1
            }
            
            func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                return issues.count
            }
            
            func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "issueCollectionViewCell", for: indexPath) as! IssuesCollectionViewCell
                
                cell.issue = issues[indexPath.item]
                
                return cell
            }
        }

     
        extension AllIssuesViewController : UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
        {
            func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>)
            {
                let layout = self.collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
                let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing

                var offset = targetContentOffset.pointee
                let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
                let roundedIndex = round(index)

                offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
                targetContentOffset.pointee = offset
            }
            
            
            func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
                return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
            }
}
