//
//  SettingsViewController.swift
//  vogue2.0
//
//  Created by Gianna Stylianou on 12/12/19.
//  Copyright © 2019 Gianna Stylianou. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    let data = ["General", "FAQ", "Privacy"]

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.separatorColor = UIColor.white
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInRows section: Int) -> CGFloat {
      return 10
  }
    
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellReuseIdentifier")!
        cell.backgroundColor = UIColor.white
        cell.layer.cornerRadius = 10
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.black.cgColor
    
        cell.translatesAutoresizingMaskIntoConstraints = false
        let text = data[indexPath.row]
        cell.textLabel?.textColor = UIColor.black
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        cell.textLabel?.translatesAutoresizingMaskIntoConstraints = false
        cell.textLabel?.text = text
        return cell
    }
}
    
    
    
