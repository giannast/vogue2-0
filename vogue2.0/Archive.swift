//
//  Archive.swift
//  vogue2.0
//
//  Created by Gianna Stylianou on 12/12/19.
//  Copyright © 2019 Gianna Stylianou. All rights reserved.
//

import UIKit


class Issue
{
    // MARK: - Public API
    var issueTitle = ""
    var issueCover: UIImage
    
    init(issueTitle: String, issueCover: UIImage)
    {
        self.issueTitle = issueTitle
        self.issueCover = issueCover
    }
    
    // MARK: - Private
    // dummy data
    static func fetchIssues() -> [Issue]
    {
        return [
            Issue(issueTitle: "December 2019", issueCover: UIImage(named: "DEC2019")!),
            Issue(issueTitle: "November 2019", issueCover: UIImage(named: "NOV2019")!),
            Issue(issueTitle: "October 2019", issueCover: UIImage(named: "OCT2019")!),
            Issue(issueTitle: "September 2019", issueCover: UIImage(named: "SEP2019")!),
            Issue(issueTitle: "August 2019", issueCover: UIImage(named: "AUG2019")!),
            Issue(issueTitle: "July 2019", issueCover: UIImage(named: "JUL2019")!),
            Issue(issueTitle: "June 2019", issueCover: UIImage(named: "JUN2019")!),
            Issue(issueTitle: "May 2019", issueCover: UIImage(named: "MAY2019")!),
            Issue(issueTitle: "April 2019", issueCover: UIImage(named: "APR2019")!),
            Issue(issueTitle: "March 2019", issueCover: UIImage(named: "MAR2019")!),
            Issue(issueTitle: "February 2019", issueCover: UIImage(named: "FEB2019")!),
            Issue(issueTitle: "January 2019", issueCover: UIImage(named: "JAN2019")!),
            Issue(issueTitle: "December 2018", issueCover: UIImage(named: "DEC2018")!),
            Issue(issueTitle: "November 2018", issueCover: UIImage(named: "NOV2018")!),
            Issue(issueTitle: "October 2018", issueCover: UIImage(named: "OCT2018")!),
            Issue(issueTitle: "September 2018", issueCover: UIImage(named: "SEP2018")!),
            Issue(issueTitle: "August 2018", issueCover: UIImage(named: "AUG2018")!),
            Issue(issueTitle: "July 2018", issueCover: UIImage(named: "JUL2018")!),
            Issue(issueTitle: "June 2018", issueCover: UIImage(named: "JUN2018")!),
            Issue(issueTitle: "May 2018", issueCover: UIImage(named: "MAY2018")!),
            Issue(issueTitle: "April 2018", issueCover: UIImage(named: "APR2018")!),
            Issue(issueTitle: "March 2018", issueCover: UIImage(named: "MAR2018")!),
            Issue(issueTitle: "February 2018", issueCover: UIImage(named: "FEB2018")!),
            Issue(issueTitle: "January 2018", issueCover: UIImage(named: "JAN2018")!),
            Issue(issueTitle: "December 2017", issueCover: UIImage(named: "DEC2017")!),
            Issue(issueTitle: "November 2017", issueCover: UIImage(named: "NOV2017")!),
            Issue(issueTitle: "October 2017", issueCover: UIImage(named: "OCT2017")!),
            Issue(issueTitle: "September 2017", issueCover: UIImage(named: "SEP2017")!),
            Issue(issueTitle: "August 2017", issueCover: UIImage(named: "AUG2017")!),
            Issue(issueTitle: "July 2017", issueCover: UIImage(named: "JUL2017")!),
            Issue(issueTitle: "June 2017", issueCover: UIImage(named: "JUN2017")!),
            Issue(issueTitle: "May 2017", issueCover: UIImage(named: "MAY2017")!),
            Issue(issueTitle: "April 2017", issueCover: UIImage(named: "APR2017")!),
            Issue(issueTitle: "March 2017", issueCover: UIImage(named: "MAR2017")!),
            Issue(issueTitle: "February 2017", issueCover: UIImage(named: "FEB2017")!),
            Issue(issueTitle: "January 2017", issueCover: UIImage(named: "JAN2017")!),
            Issue(issueTitle: "December 2016", issueCover: UIImage(named: "DEC2016")!),
            Issue(issueTitle: "November 2016", issueCover: UIImage(named: "NOV2016")!),
            Issue(issueTitle: "October 2016", issueCover: UIImage(named: "OCT2016")!),
            Issue(issueTitle: "September 2016", issueCover: UIImage(named: "SEP2016")!),
            Issue(issueTitle: "August 2016", issueCover: UIImage(named: "AUG2016")!),
            Issue(issueTitle: "July 2016", issueCover: UIImage(named: "JUL2016")!),
            Issue(issueTitle: "June 2016", issueCover: UIImage(named: "JUN2016")!),
            Issue(issueTitle: "May 2016", issueCover: UIImage(named: "MAY2016")!),
            Issue(issueTitle: "April 2016", issueCover: UIImage(named: "APR2016")!),
            Issue(issueTitle: "March 2016", issueCover: UIImage(named: "MAR2016")!),
            Issue(issueTitle: "February 2016", issueCover: UIImage(named: "FEB2016")!),
            Issue(issueTitle: "January 2016", issueCover: UIImage(named: "JAN2016")!),
        ]
    }
}

